package com.bigbank.techOffsite.controllerTests

import com.bigbank.techOffice.dto.request.HttpRequest
import com.bigbank.techOffice.exception.HttpClientException
import com.bigbank.techOffice.http.HttpClient
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.http.ContentType
import org.junit.Assert.assertThrows
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull

class HttpClientTest {

    private val TEST_GET_URL = "https://postman-echo.com/get"
    private val TEST_POST_URL = "https://postman-echo.com/post"
    private val objectMapper = ObjectMapper()

    @Test
    fun testGet() {
        val request = HttpRequest.create(
            getTestHeaders(),
            TEST_GET_URL
        )

        val getEcho = HttpClient.get(request)
        assertEquals(200, getEcho.code())
    }

    @Test
    fun testPost() {
        val request = HttpRequest.create(
            getTestHeaders(),
            TEST_POST_URL,
            ContentType.Application.Json,
            objectMapper.writeValueAsString(getTestRequestBody())
        )

        val postEcho = HttpClient.post(request)

        assertNotNull(postEcho.body())
    }

    @Test
    fun testGetThrowsException() {
        val request = HttpRequest.create(
            mutableMapOf(
                "foo" to "bar",
                "baz" to "foo"
            ),
            "httpss://testIncorrectURL.com"
        )

        assertFailsWith<HttpClientException> { HttpClient.get(request) }
    }

    @Test
    fun testThrowsHttpClientExceptionIfContentTypeIsNotSupported() {
        val request = HttpRequest.create(
            getTestHeaders(),
            TEST_POST_URL,
            ContentType.Application.Pdf,
            objectMapper.writeValueAsString(getTestRequestBody())
        )

        assertFailsWith<HttpClientException> { HttpClient.post(request) }
    }

    @Test
    fun httpClientGetThrowsExceptionWhenUrlIsEmpty() {
        val request = HttpRequest.create(
            getTestHeaders(),
            null
        )

        val exception = assertThrows(HttpClientException::class.java) { HttpClient.get(request) }

        assertEquals("API URL is empty!", exception.message)
    }

    private fun getTestRequestBody(): Map<String, String> {
        return mapOf(
            "one" to "two",
            "three" to "four",
            "five" to "six"
        )
    }

    private fun getTestHeaders(): MutableMap<String, Any> {
        return mutableMapOf(
            "foo" to "bar",
            "baz" to "foo"
        )
    }
}
