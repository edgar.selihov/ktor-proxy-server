package com.bigbank.techOffice.apiClient

import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.dto.response.memeGeneration.MemeResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.RuntimeException

object MemeGeneratorApiClient {

    private val logger: Logger = LoggerFactory.getLogger(MemeGeneratorApiClient::class.java)
    private val host: String = "https://memebuild.com/api/1.0"
    private val API_KEY: String = System.getenv("ENV_MEME_GENERATOR_API_KEY")
        ?: throw RuntimeException("Meme Generator API key is not set up")

    fun createMeme(memeRequest: MemeRequest): MemeResponse? {
        // TODO: Implement API client method for getting user's created meme
        return null
    }
}
