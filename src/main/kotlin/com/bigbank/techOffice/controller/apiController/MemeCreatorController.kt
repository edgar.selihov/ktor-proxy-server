package com.bigbank.techOffice.controller.apiController

import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.exception.RequestBodyValidationException
import io.ktor.routing.Routing
import io.ktor.routing.post

fun Routing.memeCreatorRouting() {
    post("/meme") {
    // TODO: write endpoint for creating a users' meme
    }
}

private fun validateRequestBody(request: MemeRequest) {
    when {
        request.topText.isNullOrEmpty() && request.bottomText.isNullOrEmpty() ->
            throw RequestBodyValidationException("Top text and bottom text cannot be empty!")
        request.imgUrl.isNullOrEmpty() -> throw RequestBodyValidationException("Meme's template image URL cannot be empty!")
    }
}
