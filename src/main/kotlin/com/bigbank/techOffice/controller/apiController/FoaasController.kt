package com.bigbank.techOffice.controller.apiController

import com.bigbank.techOffice.apiClient.FoaasApiClient
import com.bigbank.techOffice.dto.request.foaas.FoaasOperationRequest
import com.bigbank.techOffice.dto.response.foaas.FoaasQuoteResponse
import com.bigbank.techOffice.exception.ApiClientException
import com.bigbank.techOffice.exception.RequestBodyValidationException
import com.bigbank.techOffice.util.StringUtil
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

fun Routing.foaasRouting() {
    get("/fooas-operations") {
        val response = try {
            FoaasApiClient.getOperations()
        } catch (e: ApiClientException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(400, e.message!!)
            )
            return@get
        }

        call.respond(response)
    }

    post("/foaas-polite-quote") {
        val requestBody = call.receive<FoaasOperationRequest>()

        try {
            isValidRequestBody(requestBody)
        } catch (e: RequestBodyValidationException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(412, e.message!!)
            )
            return@post
        }

        // map of fields guarantees that it includes only not null keys and values
        val fieldsAsMap = requestBody.fields
            .associate { it.field to it.value }
            .filterKeys { it != null }
            .filterValues { it != null }

        val formattedUrl = StringUtil.formatPathParamsForFoaasQuote(requestBody.url!!, fieldsAsMap)

        val response = try {
            FoaasApiClient.getPoliteQuote(formattedUrl)
        } catch (e: ApiClientException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(400, e.message!!)
            )
            return@post
        } ?: FoaasQuoteResponse()

        call.respond(response)
    }
}

private fun isValidRequestBody(request: FoaasOperationRequest) {
    val totalRequiredParams = request.url
        ?.split("/")
        ?.filter { it.startsWith(":") }

    val uniqueFields = request.fields.distinctBy { it.field }

    when {
        request.url.isNullOrEmpty() -> throw RequestBodyValidationException("Quote URL is empty!")
        uniqueFields.isEmpty() -> throw RequestBodyValidationException("Url fields are not specified!")
        (uniqueFields.size != totalRequiredParams?.size) ->
            throw RequestBodyValidationException("Total fields required in URL and provided fields in request body do not match!")
        uniqueFields.any { !request.url!!.contains(it.field ?: "") } ->
            throw RequestBodyValidationException("Fields in URL and fields in request body do not match!")
    }
}
