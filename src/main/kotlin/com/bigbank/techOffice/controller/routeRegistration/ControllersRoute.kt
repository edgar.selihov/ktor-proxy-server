package com.bigbank.techOffice.controller.routeRegistration

import com.bigbank.techOffice.controller.apiController.apiStatusRouting
import com.bigbank.techOffice.controller.apiController.foaasRouting
import com.bigbank.techOffice.controller.apiController.tronaldDumpRouting
import com.bigbank.techOffice.controller.docs.statusRouting
import com.bigbank.techOffice.controller.docs.initOpenApiRouting
import com.bigbank.techOffice.controller.docs.tronaldDumpRouting
import com.bigbank.techOffice.controller.docs.foaasRouting
import com.bigbank.techOffice.controller.docs.memeCreatorRouting
import com.papsign.ktor.openapigen.route.apiRouting
import io.ktor.application.Application
import io.ktor.routing.routing

/**
 * A method for registering endpoints' routes
 **/
fun Application.registerRoutes() {
    // TODO: Add FOASS, meme creator endpoints' routes here
    routing {
        initOpenApiRouting()
        apiStatusRouting()
        foaasRouting()
    }

    /* Open API docs routing */
    apiRouting {
        statusRouting()
        tronaldDumpRouting()
        foaasRouting()
        memeCreatorRouting()
    }
}
