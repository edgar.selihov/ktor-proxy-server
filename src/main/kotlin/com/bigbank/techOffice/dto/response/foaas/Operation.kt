package com.bigbank.techOffice.dto.response.foaas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class Operation {
    @JsonProperty("name")
    var name: String? = null

    @JsonProperty("url")
    var url: String? = null

    @JsonProperty("fields")
    var fields: List<Field> = listOf()
}
