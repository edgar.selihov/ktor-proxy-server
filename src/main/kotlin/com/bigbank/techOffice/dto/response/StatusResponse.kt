package com.bigbank.techOffice.dto.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class StatusResponse {
    var message: String? = null
    var code: Long? = null
}
