package com.bigbank.techOffice.dto.response.foaas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class FoaasQuoteResponse {
    var message: String? = null
    var subtitle: String? = null
}
