package com.bigbank.techOffice.dto.request.foaas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class FoaasFieldRequest {
    var field: String? = null
    var value: String? = null
}
