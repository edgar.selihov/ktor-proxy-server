package com.bigbank.techOffice.dto.request

import io.ktor.http.ContentType

class HttpRequest private constructor() {
    var headers: Map<String, Any> = mutableMapOf()
    var url: String? = null
    var body: String? = null
    var contentType: ContentType? = null

    companion object Factory {
        fun create(
            headers: MutableMap<String, Any>,
            url: String?,
            contentType: ContentType? = null,
            body: String? = null // default value
        ): HttpRequest {
            return HttpRequest().also {
                it.headers = headers
                it.url = url
                it.body = body
                it.contentType = contentType
            }
        }
    }
}
