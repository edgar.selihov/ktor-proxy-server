package com.bigbank.techOffice.exception

import java.lang.RuntimeException

class RequestBodyValidationException : RuntimeException {
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable) : super(message, cause)
}
