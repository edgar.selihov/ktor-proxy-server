package com.bigbank.techOffice.http

import com.bigbank.techOffice.dto.request.HttpRequest
import com.bigbank.techOffice.exception.HttpClientException
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.rybalkinsd.kohttp.dsl.httpGet
import io.github.rybalkinsd.kohttp.dsl.httpPost
import io.github.rybalkinsd.kohttp.ext.url
import io.ktor.http.ContentType
import java.io.IOException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.UnsupportedOperationException
import kotlin.jvm.Throws

object HttpClient {

    private val objectMapper = ObjectMapper()
    private val logger: Logger = LoggerFactory.getLogger(HttpClient::class.java)

    @Throws(HttpClientException::class)
    fun get(request: HttpRequest) = try {
        logger.info("Sending GET request to ${request.url}")

        if (request.url.isNullOrEmpty()) {
            throw HttpClientException("API URL is empty!")
        }

        httpGet {
            url(request.url!!)
            header {
                request.headers.map { header -> header.key to header.value }
            }
        }
    } catch (e: IOException) {
        throw HttpClientException("GET request failed: ${e.message}", e)
    }

    @Throws(HttpClientException::class)
    fun post(request: HttpRequest) = try {
        logger.info("Sending POST request to ${request.url}")

        if (request.url.isNullOrEmpty()) {
            throw HttpClientException("API URL is empty!")
        }

        httpPost {
            url(request.url!!)

            header {
                request.headers.map { header -> header.key to header.value }
            }

            body {
                when (request.contentType) {
                    ContentType.Application.Json -> json(objectMapper.writeValueAsString(request.body))
                    ContentType.MultiPart.FormData -> form(request.body!!)
                    else -> throw UnsupportedOperationException("Unsupported content type: ${request.contentType}")
                }
            }
        }
    } catch (e: IOException) {
        throw HttpClientException("POST request failed: ${e.message}", e)
    } catch (e: UnsupportedOperationException) {
        throw HttpClientException("POST request failed: ${e.message}", e)
    }
}
