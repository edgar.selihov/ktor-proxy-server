# Ktor proxy server

## Required tools for development and booting up Ktor proxy server
* [JDK 11](https://jdk.java.net/java-se-ri/11)
* [Gradle 7.2](https://gradle.org/install/)
* [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community edition)
* [Postman](https://www.postman.com/downloads/)
* [Kotlin](https://plugins.jetbrains.com/plugin/6954-kotlin) plugin from IntelliJ marketplace after IntelliJ IDEA installation. For a help refer to this [link](https://www.jetbrains.com/help/idea/managing-plugins.html)

## Before starting up:
1. Run `git submodule update --init --recursive` before building the project to pull submodule.

### To run the project with Gradle for `MacOS/Linux`:

Build the project:
* `./gradlew clean build`

Run tests:
* `./gradlew test`

Run the project:
* `./gradlew run`

### To run the project with Gradle for `Windows`:

Build the project:
* `gradlew clean build`

Run tests:
* `gradlew test`

Run the project:
* `gradlew run`

Ktor proxy server will be running at `http://localhost:6001`

For documentation go to `http://localhost:6001/docs`

NB! If you are getting `TypeError` because of CORS policy in Chrome when trying to get example endpoint's response in swagger, try to switch the browser to Firefox or Safari. 

For a better usability import `ktor.postman_collection.json` collection file into Postman (https://learning.postman.com/docs/getting-started/importing-and-exporting-data/)
